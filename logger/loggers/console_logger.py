from logger.abstract_classes.abstract_logger import AbstractLogger
from logger.configs.console_logger_config import ConsoleLoggerConfig
from logger.utils.logging_level import LoggingLevel


class ConsoleLogger(AbstractLogger):

    def __init__(self, config: ConsoleLoggerConfig):
        super(ConsoleLogger, self).__init__(config)

    def log(self, log_level: LoggingLevel, log_name: str, message: dict):
        if log_level.value < self._min_log_level.value:
            return
        if log_level.value == LoggingLevel.DEBUG_HIGH.value:
            print(message)
            return
        if log_level.value == LoggingLevel.DEBUG.value:
            print(message)
            return
        if log_level.value == LoggingLevel.INFO.value:
            print(message)
            return
        if log_level.value == LoggingLevel.WARN.value:
            print(message)
            return
        if log_level.value == LoggingLevel.ERROR.value:
            print(message)
            return
        if log_level.value == LoggingLevel.FATAL.value:
            print(message)
            return