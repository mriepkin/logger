from logger.abstract_classes.abstract_logger import AbstractLogger
from typing import List
from logger.utils.logging_level import LoggingLevel


class AbstractLoggerManager:
    _active_loggers: List[AbstractLogger] = []
    _prefix = ''

    @staticmethod
    def add_logger(logger: AbstractLogger):
        AbstractLoggerManager._active_loggers.append(logger)

    @staticmethod
    def set_prefix(prefix: str):
        AbstractLoggerManager._prefix = prefix

    @staticmethod
    def log(log_level: LoggingLevel, log_name: str, message: dict):
        for logger in AbstractLoggerManager._active_loggers:
            try:
                logger.log(log_level, AbstractLoggerManager._prefix+log_name, message)
            except Exception as e:
                print(str(e))
                continue
