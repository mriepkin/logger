#from distutils.core import setup
import setuptools

with open('requirements.txt') as f:
    required = f.read().splitlines()

setuptools.setup(
    name="logger",
    version="0.04",
    description="This is framework for logging",
    author="Maksym Riepkin",
    author_email="maksym.riepkin@mycredit.ua",
    packages=[
        'logger',
        'logger.abstract_classes',
        'logger.configs',
        'logger.loggers',
        'logger.utils'
    ],
    install_requires=required,
    dependency_links=['git+ssh://git@bitbucket.org/mriepkin/kafkamessagesystem.git']
)

