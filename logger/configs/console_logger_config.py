from logger.abstract_classes.abstract_logger import AbstractLoggerConfig
from logger.utils.logging_level import LoggingLevel


class ConsoleLoggerConfig(AbstractLoggerConfig):
    def __init__(self, min_log_level: LoggingLevel):
        super(ConsoleLoggerConfig, self).__init__(min_log_level)
