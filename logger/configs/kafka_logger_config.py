from logger.abstract_classes.abstract_logger import AbstractLoggerConfig
from logger.utils.logging_level import LoggingLevel


class KafkaLoggerConfig(AbstractLoggerConfig):
    def __init__(self, min_log_level: LoggingLevel, topics: dict):
        super(KafkaLoggerConfig, self).__init__(min_log_level)
        self.topics = topics
