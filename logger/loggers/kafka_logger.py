from logger.abstract_classes.abstract_logger import AbstractLogger
from logger.configs.kafka_logger_config import KafkaLoggerConfig
from logger.utils.logging_level import LoggingLevel
from kafka_message_system.abstract_classes import AbstractProducer


class KafkaLogger(AbstractLogger):

    def __init__(self, config: KafkaLoggerConfig, producer: AbstractProducer):
        super(KafkaLogger, self).__init__(config)
        self._topics = config.topics
        self._producer = producer

    def log(self, log_level: LoggingLevel, log_name: str, message: dict):
        if log_level.value < self._min_log_level.value:
            return
        message['LogType'] = log_name
        if log_level.value == LoggingLevel.DEBUG_HIGH.value:
            message['LogLevel'] = 'DEBUG_HIGH'
            self._producer.produce_message(self._topics['default'], message)
            return
        if log_level.value == LoggingLevel.DEBUG.value:
            message['LogLevel'] = 'DEBUG'
            self._producer.produce_message(self._topics['default'], message)
            return
        if log_level.value == LoggingLevel.INFO.value:
            message['LogLevel'] = 'INFO'
            self._producer.produce_message(self._topics['default'], message)
            return
        if log_level.value == LoggingLevel.WARN.value:
            message['LogLevel'] = 'WARN'
            self._producer.produce_message(self._topics['default'], message)
            return
        if log_level.value == LoggingLevel.ERROR.value:
            message['LogLevel'] = 'ERROR'
            self._producer.produce_message(self._topics['default'], message)
            return
        if log_level.value == LoggingLevel.FATAL.value:
            message['LogLevel'] = 'FATAL'
            self._producer.produce_message(self._topics['default'], message)
            return
