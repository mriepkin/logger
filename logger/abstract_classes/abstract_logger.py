from abc import ABC, abstractmethod
from logger.utils.logging_level import LoggingLevel


class AbstractLoggerConfig:
    def __init__(self, min_log_level: LoggingLevel):
        self.min_log_level = min_log_level


class AbstractLogger(ABC):
    """
    Base class for all loggers
    """
    def __init__(self, config: AbstractLoggerConfig):
        self._min_log_level = config.min_log_level

    @abstractmethod
    def log(self, log_level: LoggingLevel, log_name: str, message: dict):
        pass
